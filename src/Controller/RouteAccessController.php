<?php
/**
 * @file
 * Contains \Drupal\route_access\Controller\RouteAccessController.
 */

namespace Drupal\route_access\Controller;

use Drupal\Core\Access\AccessManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Route;

class RouteAccessController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * @var PrivateTempStoreFactory
   */
  protected $privateTempStore;

  /**
   * @param \Drupal\Core\Render\RendererInterface $renderer
   * @param \Drupal\user\PrivateTempStoreFactory $privateTempStore
   */
  public function __construct(RendererInterface $renderer, PrivateTempStoreFactory $privateTempStore) {
    $this->renderer = $renderer;
    $this->privateTempStore = $privateTempStore;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('user.private_tempstore')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function title() {
    return $this->t('Access results');
  }

  /**
   * @return static
   */
  public static function batch() {
    return RedirectResponse::create(Url::fromRoute('route_access.results')->toString());
  }

  public function build() {
    $build = [];

    $private_temp_store = $this->privateTempStore->get('route_access_check');
    $results = $private_temp_store->get('results');

    if (!$results) {
      return $build;
    }

    /** @var User $user */
    $user = $private_temp_store->get('user');

    $rows = [];
    $responses = [];

    foreach ($results as $index => $result) {
      /** @var AccessResult $response */
      $response = $result['response'];

      /** @var Route $route */
      $route = $result['route'];

      $response_type = $this->getResponseType($response, $result['resolved']);

      if (!isset($responses[$response_type])) {
        $responses[$response_type] = 0;
      }
      $responses[$response_type]++;

      $route_title = $route->getDefault('_title');

      $path = [
        // @TODO support _title_callback.
        '#suffix' => !is_null($route_title) ?  ' (<b>' . $route_title . '</b>)' : '',
      ];


      $path['#markup'] = $route->getPath();
//      if ($result['resolved'] == FALSE) {
//        $path['#markup'] = $route->getPath();
//      }
//      else {
//        $route_path = $route->getPath();
//
//        $path['#type'] = 'link';
//        $path['#title'] = $route_path;
//        $path['#url'] = Url::fromRoute($result['name']);
//      }

      switch ($response_type) {
        case 'forbidden':
          $row_class = 'color-error';
          break;
        case 'allowed':
          $row_class = 'color-success';
          break;
        default:
          $row_class = 'color-warning';
      }

      $rows[] = [
        'class' => [
          $row_class,
        ],
        'data' => [
          'id' => [
            'data' => [
              '#markup' => $index,
            ],
          ],
          'path' => [
            'data' => $path,
          ],
          'reason' => [
            'data' => [
              '#markup' => !$response instanceof AccessResultAllowed ? $response->getReason() : '',
            ],
          ],
          'response' => [
            'data' => [
              '#markup' => $response_type,
            ],
          ],
        ],
      ];
    };

    $counts = array_map(function($response, $value) {
      return $this->t('@response_type count: @count.', [
        '@response_type' => $response,
        '@count' => $value,
      ]);
    }, array_keys($responses), array_values($responses));

    $build['table'] = [
      '#type' => 'table',
      '#caption' => $this->t('Access check results for user @user (@uid)', [
        '@user' => $user->label(),
        '@uid' => $user->id(),
      ]),
      '#header' => [
        $this->t('Id'),
        $this->t('Path'),
        $this->t('Reason (if not allowed)'),
        $this->t('Response'),
      ],
      '#prefix' => $this->t('Responses: @responses', [
        '@responses' => implode(' | ', $counts),
      ]),
      '#rows' => $rows,
    ];

    return $build;
  }

  /**
   * @param \Drupal\Core\Access\AccessResult $access
   * @param $resolved
   *
   * @return string
   */
  public function getResponseType(AccessResult $access, $resolved) {
    if ($resolved == FALSE) {
      return 'neutral';
    }

    if ($access->isAllowed()) {
      return 'allowed';
    }
    else {
      return 'forbidden';
    }
  }

  /**
   * @param \Symfony\Component\Routing\Route $route
   * @param string $route_name
   * @param array $route_parameters
   */
  public static function checkAccess(Route $route, $route_name, $route_parameters = []) {
    /** @var PrivateTempStoreFactory $private_temp_store */
    $private_temp_store = \Drupal::service('user.private_tempstore')->get('route_access_check');

    /** @var AccessManager $route_access_manager */
    $route_access_manager = \Drupal::service('access_manager');

    /** @var User $user */
    $user = $private_temp_store->get('user');

    $path = $route->getPath();

    $result = [
      'path' => $path,
      'name' => $route_name,
      'route' => $route,
      'resolved' => TRUE,
    ];

    // @TODO generate route parameter combinations and call this function recursively. Do not forget about memory limits as this is called in one batch execution time.
    try {
      $result['response'] = $route_access_manager->checkNamedRoute($route_name, $route_parameters, $user, TRUE);
    }
    catch (\Exception $e) {
      $result['response'] = AccessResult::neutral($e->getMessage());
      $result['resolved'] = FALSE;
    }
    catch (\TypeError $e) {
      $result['response'] = AccessResult::neutral($e->getMessage());
      $result['resolved'] = FALSE;
    }

    // Update previous result set with the new result.
    $stored_results = $private_temp_store->get('results');
    $stored_results[] = $result;
    $private_temp_store->set('results', $stored_results);
  }

}