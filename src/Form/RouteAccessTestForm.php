<?php

namespace Drupal\route_access\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteProvider;
use Drupal\user\Entity\User;
use Drupal\user\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Todo: me.
 */
class RouteAccessTestForm extends FormBase {

  /**
   * @var PrivateTempStoreFactory
   */
  protected $privateTempStore;

  /**
   * @var RouteProvider
   */
  protected $routeProvider;

  /**
   * @param \Drupal\user\PrivateTempStoreFactory $privateTempStore
   * @param \Drupal\Core\Routing\RouteProvider $routeProvider
   */
  public function __construct(PrivateTempStoreFactory $privateTempStore, RouteProvider $routeProvider) {
    $this->privateTempStore = $privateTempStore;
    $this->routeProvider = $routeProvider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.private_tempstore'),
      $container->get('router.route_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'route_access_test_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['user_id'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#default_value' => NULL,
      '#selection_settings' => ['include_anonymous' => TRUE],
      '#title' => $this->t('Select user'),
      '#description' => 'Leave blank for anonymous',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Run',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user_id = $form_state->getValue('user_id') ?: 0;

    $private_temp_store = $this->privateTempStore->get('route_access_check');
    // Empty results from previous access checks or set to an array if it's the first time access check is used.
    $private_temp_store->set('results', []);
    // Also store user id for later use.
    $private_temp_store->set('user', User::load($user_id));

    $batch_operations = [];

    /** @var \Symfony\Cmf\Component\Routing\PagedRouteCollection $paged */
    $paged_routes = $this->routeProvider->getAllRoutes();
    $paged_routes->rewind();

    for ($i = 0; $i <= $paged_routes->count(); $i++) {
      $current_route = $paged_routes->current();
      $current_route_key = $paged_routes->key();
      if ($current_route === FALSE) {
        continue;
      }
      $paged_routes->next();

      $batch_operations[] = [
        '\Drupal\route_access\Controller\RouteAccessController::checkAccess',
        [
          $current_route,
          $current_route_key,
        ],
      ];
    }

    batch_set([
      'title' => $this->t('Checking access'),
      'operations' => $batch_operations,
      'finished' => '\Drupal\route_access\Controller\RouteAccessController::batch',
    ]);
  }

}